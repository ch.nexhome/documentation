
# Overwiew

![alt text](https://github.com/jorisbaiutti/BTI7252/blob/master/03_Applications/Application-overview.svg "Systemübersicht")



# Application and automation control unit

This component represents the brain of the kanu club house. Automations, scheduled processes or UI actions are managed by this controller.

## Tool
The Controller is build of [node-red](https://nodered.org) flows and can be easily expanded. The access to the programming interface is protected by basic authentication (please refere to the oneNote).

## Api
The controller has an REST interface to receive commands. It is accessible only locally.
The Api reference can be found [here](controller/api_reference.rst)


## Use Cases
Currently one use case is implemented.

* Humidity fan control 

![Fan controller](controller/media/controller.png "Fan controller")
